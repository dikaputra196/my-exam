<?php
	require_once dirname(__FILE__).'/config/config.php';
?>
<!DOCTYPE html>
<html>
<head>
	<title>Ujian Akhir Semester CA163</title>
	<!-- Bootstrap -->
	<link rel="stylesheet" type="text/css" href="<?= MAIN_URL ?>/components/bootstrap/css/bootstrap.css">
	<!-- Font Awesome -->
	<link rel="stylesheet" type="text/css" href="<?= MAIN_URL ?>/components/font_awesome/css/font-awesome.css">
	<!-- My Own CSS -->
	<style type="text/css">
		.confluid {
	      padding-top:30px;
	      padding-bottom:30px;
  		}
  		.bg-1 { 
	      background-color: #1abc9c; /* Green */
	      color: #ffffff;
	  	}
	  	.bg-2 { 
	      background-color: #474e5d; /* Dark Blue */
	      color: #ffffff;
	  	}
	  	.bg-3 { 
	      background-color: #ffffff; /* White */
	      color: #555555;
	  	}
	  	.bg-4 { 
	      background-color: #2f2f2f; /* Black Gray */
	      color: #fff;
	  	}
		p{
			font-size: 16px;
		}
		.margin{
			margin-bottom:20px;
		}
		.bg-navy{
			background-color:#00004C;
			color:#FFFFFF;
		}
		.control-label{
			align:right;
			font-size:100%;
		}
	</style>

</head>
<body>
	<!-- Top Navbar -->
	<nav class="navba navbar-default">
		<div class="container">
			<div class="navbar-header">
				<a href="#" class="navbar-brand">Soal Type 1</a>
			</div>
			<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-right">
				<li><a href="<?= MAIN_URL ?>/index.php">HOME</a></li>
				<li><a href="#">ABOUT</a></li>
				<li><a href="#">CONTACT</a></li>
			</ul>
		</div>
		</div>
	</nav>
	<!-- End Top Nav -->

	<!-- Wrapper -->
	<div class="col-md-12">
	
	<!-- Home Sub Heading -->
	<div class="row col-md-2"></div>
	<div class="row">
		<header class="header">
			<div class="confluid bg-1 col-md-8">
				<h3 class="margin">Hello July</h3>	
			</div>
		</header>
	</div>
	<!-- End Home Sub Heading -->
	
	<!-- Breadcumb -->
	<div class="row col-md-2"></div>
	<div class="row">
		<div class="navbar navbar-default col-md-8">
			<div class="collapse navbar-collapse">
			<ul class="nav navbar-nav navbar-default">
				<li><a href="<?= MAIN_URL ?>/index.php">HOME</a></li>
			</ul>
		</div>
		</div>
	</div>
	<!-- End Breadcumb -->

	<!-- Grid Content Row 2 -->
	<div class="row col-md-2"></div>
	<div class="row">
		<!-- Grid 1 -->
		<div class="col-md-4 bg-2 confluid">
			<h3 class="margin">Have a Landscape Photo? Follow this Tips!</h3>
			<p>
				Hey guys! Now i'll share tips with you, how to edit your landscape photo more beautiful!
				<br>
				<a href="#" style="text-decoration: none; color:#FFFFFF">Check this out!  </a>
			</p>
		</div>
		<!-- End Grid 1 -->

		<!-- Grid 2 -->		
		<div class="col-md-5">
			<img src="<?= MAIN_URL ?>/components/images/landscape.jpg" class="col-sm-10">
		</div>
		<!-- End Grid 2 -->
	</div>

	<div class="row"></div>

	<!-- Judul -->
		<div class="row col-md-2"></div>
		<div class="row">
			<div class="col-md-8"><h3 class="margin">What I have Write?</h3></div>
		</div>
	<!-- End Judul -->


	<!-- Content with 4 Grid -->
		<div class="row col-md-2"></div>
		<div class="row">
			<!-- Grid 1 -->
				<div class="col-md-2 confluid bg-1">
					<h4 class="margin text-center">Senja Hari</h4>

					<p>
						Berkisah tentang seseorang yang sedang menanti Senja
					</p>

					<div align="center">
						<button class="btn btn-success butt">Baca Sekarang</button>
					</div>
				</div>
			<!-- End Grid 1 -->
			<!-- Grid 2 -->
				<div class="col-md-2 confluid bg-2">
					<h4 class="margin text-center">Dikala Nonton Televisi</h4>

					<p>
						Sedih, Senang, Cinta, kisah ku ketika menonton Televisi
					</p>

					<div align="center">
						<button class="btn btn-success butt">Baca Sekarang</button>
					</div>
				</div>
			<!-- End Grid 2 -->
			<!-- Grid 3 -->
				<div class="col-md-2 confluid bg-4">
					<h4 class="margin text-center">Aku Tidak Seviral Dia</h4>

					<p>
						Menceritakan seorang gadis yang bercita - cita menjadi Selebgram
					</p>

					<div align="center">
						<button class="btn btn-success butt">Baca Sekarang</button>
					</div>
				</div>
			<!-- End Grid 3 -->
			<!-- Grid 4 -->
				<div class="col-md-2 confluid bg-1">
					<h4 class="margin text-center">Kuliah <strike>Seindah</strike> Sinetron</h4>

					<p>
						Berkisah tentang Alit, seorang mahasiswa baru di suatu Kampus ternama
					</p>

					<div align="center">
						<button class="btn btn-success butt">Baca Sekarang</button>
					</div>
				</div>
			<!-- End Grid 4 -->
		</div>
	<!-- End Content with 4 Grid -->	

	<!-- Limit -->
		<div class="row col-md-2"></div>
		<div class="row">
			<div class="col-md-8"><h3 class="margin">Others</h3></div>
		</div>
	<!-- Limit -->

		<!-- 2nd Content with 4 Grid -->
		<div class="row col-md-2"></div>
		<div class="row">
			<!-- Grid 1 -->
				<div class="col-md-2 confluid bg-1">
					<h4 class="margin text-center">Sahabatku</h4>

					<p>
						Will add soon
					</p>
				</div>
			<!-- End Grid 1 -->
			<!-- Grid 2 -->
				<div class="col-md-2 confluid bg-2">
					<h4 class="margin text-center">Dia</h4>

					<p>
						Will add soon
					</p>
				</div>
			<!-- End Grid 2 -->
			<!-- Grid 3 -->
				<div class="col-md-2 confluid bg-4">
					<h4 class="margin text-center butt">Hikmah</h4>

					<p>
						Will add soon
					</p>
				</div>
			<!-- End Grid 3 -->
			<!-- Grid 4 -->
				<div class="col-md-2 confluid bg-1">
					<h4 class="margin text-center">Mukzizat</h4>

					<p>
						Will add soon
					</p>
				</div>
			<!-- 2nd End Grid 4 -->
		</div>
	<!-- End Content with 4 Grid -->

	<!-- Limit -->
		<div class="row col-md-2"></div>
		<div class="row">
			<div class="col-md-8"><h5 class="margin"></h5></div>
		</div>
	<!-- Limit -->

	<!-- Footer -->
	<div class="row">
		<footer class="main-footer">
			<div class="bg-4 confluid text-center">
				<small class="text-center">Ujian Akhir Semester</small> 
				<div><small>Made by : Andika Putra Dinata</small></div>
			</div>
		</footer>
	</div>
	<!-- End Footer -->

	</div>
	<!-- End Wrapper -->

</body>
<!-- jQuery -->
<script src="<?= MAIN_URL ?>/components/jQuery/jquery-3.2.1.js"></script>
<!-- Bootstrap -->
<script src="<?= MAIN_URL ?>/components/bootstrap/js/bootstrap.js"></script>
<!-- Sweetalert -->
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>

<!-- Script Place -->
<script>
	$(document).ready(function(){
		$(".butt").click(function(){
			swal("Sorry :(", 
					"Content Saat ini Belum Dapat Dibaca", 
				"error");
		});
	});
</script>


</html>